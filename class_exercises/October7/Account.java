public class Account {
    //instance variables
    private double balance;

    //constructor
    public Account(double initialBalance) {
        balance = initialBalance;
    }

    //method that changes the balance
    //set method
    public void setBalance(double amount) {
        balance += amount;
    }

    //method return the balance
    //get method
    public double getBalance() {
        return balance;
    }
}
