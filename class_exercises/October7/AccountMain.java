public class AccountMain {
    public static void main (String [] args) {
        // create an instance of Account
        Account JoeSmith = new Account(500);
        Account BruceWayne = new Account(200);
        Account HalJordan = new Account(100);
        //call methods
        System.out.println("Balance : "+JoeSmith.getBalance());
        System.out.println("Balance : "+BruceWayne.getBalance());
        System.out.println("Balance : "+HalJordan.getBalance());

        JoeSmith.setBalance(1000);
        System.out.println("Balance : "+JoeSmith.getBalance());

        JoeSmith.setBalance(-10);
        System.out.println("Balance : "+JoeSmith.getBalance());

        BruceWayne.setBalance(50);
        System.out.println("Balance : "+BruceWayne.getBalance());

    }
}
