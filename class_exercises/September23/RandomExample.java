
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Class Exercise
//Date : 09 23 2015
//Diptajyoti Mukherjee
//Purpose : To use random class
//*********************************************
import java.util.Date;
import java.util.Random;
public class RandomExample {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n" + new Date() +"\n");
        int randomInt;
        float randomFloat;

        Random rand = new Random();
        randomInt = rand.nextInt();
        System.out.println("Random integer : "+ randomInt);
        randomInt = rand.nextInt(10);
        System.out.println("Random integer : "+ randomInt);


        randomFloat = rand.nextFloat();
        System.out.println("Random Float : "+ randomFloat);
 	}
}

