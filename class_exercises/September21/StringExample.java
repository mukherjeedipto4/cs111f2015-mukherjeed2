//==========================================
// Janyl Jumadinova
// Class Exercise
// September 21, 2015
//
// Purpose: This program uses various methods
// of the String class
// ==========================================
public class StringExample
{

   public static void main(String args[])
   {
      // declare a variable named word of type String
      String word;

      //assign the string to the variable:
      word = "AlexAnder";

      //perform some actions on the string:

      //1. retrieve the length by calling the
      //length method:
      int length = word.length();
      System.out.println("Length: " + length);

      //2. get the last character of the string
      //by calling charAt method
    //
      char letter = word.charAt(length-1);
      System.out.println("Letter: "+letter);

      //3. replace 'A' by 'a'
      System.out.println("New word : "+ word.replace('A','a'));

      //4.  return substring
      System.out.println( "Substring : " + word.substring(4));
      System.out.println( "Another Substring :"+ word.substring(0,4) );

      //5. check if two strings are the same
      String word2 = new String ("Alexander");
      System.out.println("Equals : " + word.equalsIgnoreCase(word2));

   }
}
