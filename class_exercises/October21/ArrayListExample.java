import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class ArrayListExample {
    public static void main (String args []) throws IOException {
        //create a list to store elements from file
        ArrayList<String> twitterList = new ArrayList<String>();

        //reading a text file
        File file = new File("words.txt");
        Scanner scan = new Scanner(file);


        while(scan.hasNext()){
            twitterList.add(scan.next());
        }
        System.out.println(twitterList);


    int count = 0;
    while(count < twitterList.size()) {
        String word = twitterList.get(count);
        if(word.endsWith("s")) {
            twitterList.remove(count);
        }
        count++;
    }
        System.out.println(twitterList);
    }
}
