
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Class Exercise
//Date : 09 28 2015
//Diptajyoti Mukherjee
//Purpose : To demonstrate math class example
//*********************************************
import java.util.Date;
import java.util.Scanner;
public class MathExample {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n" + new Date() +"\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Please Enter a number : ");
        double value = input.nextDouble();


        System.out.println("Absolute value : "+ Math.abs(value));
        System.out.println("Floor : "+Math.floor(value));
        System.out.println("Square Root : "+Math.sqrt(value));
        System.out.println("Pi : "+Math.PI);
 	}
}

