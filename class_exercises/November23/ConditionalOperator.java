public class ConditionalOperator {
    public static void main(String [] args) {
        int [] numbers = {100,10,234,341,676,858,94,443,903,918,89,101,575,856};
        int max = numbers[0];
        int min = numbers[0];
        //finding the maximum value and minimum value
        for(int i : numbers) {
            max = (max>i)?max:i;
            min = (min<i)?min:i;
        }
        System.out.println("maximum number is : "+max);
        System.out.println("minimum number is : "+min);
    }
}
