import java.util.Scanner;
public class CalculatorMain {
    public static void main (String [] args) {
        Scanner input = new Scanner(System.in);
        double left , right;
        char op;

        System.out.println("Enter a simple expression: ");
        left = input.nextDouble();
        op = input.next().charAt(0);
        right = input.nextDouble();

        System.out.println("You entered: "+left+op+right);

        Calculator calc = new Calculator();
        System.out.println("Your result is: "+calc.calculate(left,op,right));
    }
}
