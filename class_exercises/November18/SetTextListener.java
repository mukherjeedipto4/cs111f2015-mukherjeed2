import javax.swing.*;
import java.awt.event.*;

public class SetTextListener implements ActionListener {
    private JLabel label;
    private JTextField text;

    //constructor
    public SetTextListener(JTextField t, JLabel l) {
        label = l;
        text = t;
    }

    public void actionPerformed (ActionEvent event) {
        label.setText(text.getText());

    }

}
