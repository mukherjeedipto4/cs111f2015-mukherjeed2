
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Class Exercise September16 
//Date : 09 16 2015
//Diptajyoti Mukherjee
//Purpose : To get input from the user, manipulate it and output the result.
//*********************************************
import java.util.Date; 
import java.util.Scanner;
public class ConversionExample {
	public static void main ( String [] args ) {
		Scanner userInput = new Scanner(System.in);
		int num1 , num2;
		float num3;
		System.out.println("Name: Diptajyoti Mukherjee\n Class Exercise : September 16\n " + new Date() +"\n");
		//getting input from user
		System.out.println("Please enter an interger");
		num1 = userInput.nextInt();
		System.out.println("Please enter another integer");
		num2 = userInput.nextInt();
		System.out.println("Please enter any number");
		num3 = userInput.nextFloat();

		System.out.println("Arithmetic result 1: "+(num1*num2+num3));
		System.out.println("Arithmetic result 2: "+(num1*num2+(int)num3));
		
		System.out.println("Arithmetic result 3: "+(int)(num1*num2+num3));
		System.out.println("Last Digit " + (num1%10));
		System.out.println("Last Digit Float" + (int)(num3%10));

	}
} 
		
