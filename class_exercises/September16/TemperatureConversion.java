
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Class Exercise 
//Date : 09 16 2015
//Diptajyoti Mukherjee
//Purpose : To get an temperature input from user in Farenheits and convert it to celcius 
//*********************************************
import java.util.Date;
import java.util.Scanner; 
public class TemperatureConversion {
	public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n Class Exercise\n " + new Date() +"\n");
		Scanner userInput = new Scanner(System.in);
		float tempFarenheit;
		float tempCelcius;
		System.out.println("Please enter the temperature in Farenheits");
		tempFarenheit = userInput.nextFloat();
		tempCelcius = (tempFarenheit-32)*5/9;
		System.out.println("The temperature in celcius is : " + tempCelcius);
 	}
} 
		
