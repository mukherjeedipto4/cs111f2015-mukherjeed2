/**
 * A simple class to experiment with Swing graphics
 */

import javax.swing.*;
import java.awt.*;

public class GraphicsTester {
	public static void main(String [] args)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new FlowLayout());
		frame.add(new JButton("I'm a JButton"));
		frame.add(new JTextField("I'm a JTextField"));
		frame.add(new JLabel("I'm a JLabel"));
		frame.add(new JSlider());
		frame.add(new JProgressBar());


		JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2,2));
        panel.add(new JButton("Panel JButton"));
        panel.add(new JLabel("Inside panel"));
        panel.add(new JTextField("Inside panel"));
        panel.add(new JButton("another button"));
        frame.add(panel);

        frame.pack();
		frame.setVisible(true);
	}
}
