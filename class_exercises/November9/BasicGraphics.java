import javax.swing.*;
import java.awt.*;

public class BasicGraphics extends JApplet {
    public static void main(String [] args) {
        JFrame window = new JFrame("Window");
        window.getContentPane().add(new BasicGraphics());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
        window.setSize(600,400);

    }

    public void paint (Graphics g) {
        g.setColor(Color.red);
        g.fillRect(10,20,40,40);
        g.setColor(Color.yellow);
        g.fillOval(100,100,100,100);
    }
}
