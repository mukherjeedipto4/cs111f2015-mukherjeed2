
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Practical 10
//Date : 11 20 2015
//Diptajyoti Mukherjee
//Purpose : To make a simple conversion Program
//*********************************************
import java.util.Date;
import javax.swing.*;
import java.awt.*;
public class ConversionGUI {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n" + new Date() +"\n");
        JFrame frame = new JFrame("Convert!");
        frame.setLayout(new GridLayout(0,1));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panelConvert1 = new JPanel();
        frame.add(panelConvert1);
        panelConvert1.setBackground(Color.cyan);
        JButton convertFarToCel = new JButton("Convert!");
        panelConvert1.setLayout(new FlowLayout());
        JTextField farToCel = new JTextField("Enter F");
        JLabel farToCelLabel = new JLabel("Enter a temperature in Farenheit");
        JLabel displayCel = new JLabel("Temp. in C is : ");
        panelConvert1.add(farToCelLabel);
        panelConvert1.add(farToCel);
        panelConvert1.add(convertFarToCel);
        panelConvert1.add(displayCel);

        convertFarToCel.addActionListener(new SetTextListener(farToCel,displayCel));
        frame.setVisible(true);
        frame.pack();

 	}
}

