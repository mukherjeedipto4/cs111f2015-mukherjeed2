import javax.swing.*;
import java.awt.event.*;

public class SetTextListener implements ActionListener {
    private JLabel label;
    private JTextField text;

    public SetTextListener(JTextField t, JLabel l ) {
        label = l;
        text = t;
    }
    public void actionPerformed(ActionEvent event) {
        String t = text.getText();
        double far = Double.parseDouble(t);
        double cel = (5.0/9.0)*(far-32);
        label.setText("Temp. in C is : "+cel);
    }
}
