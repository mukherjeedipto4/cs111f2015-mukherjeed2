
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Practical 07
//Date : 10 23 2015
//
//Purpose : To create a game in which the user guesses a number between 1 to 100
//*********************************************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;
public class RandomGame {
    public static void main ( String [] args ) {
        System.out.println("Name: Diptajyoti Mukherjee\n Practical #07\n " + new Date() +"\n");
        Scanner input = new Scanner(System.in);
        Random rand = new Random();                 //random generator
        int randNum = rand.nextInt(100)+1;          //generating numbers between 1 and 100
        int userGuess = 0;                          //initialising user input
        int count = 0;                              //initialising the count
        System.out.println("Welcome to the guessing game!");
        while(userGuess!=randNum) {                 //repeating until user gets actual value and printing a hint
            System.out.println("Please enter a number between 1 and 100");
            userGuess = input.nextInt();
            if(userGuess > randNum) {
                System.out.println("Sorry, your guess was higher than the actual number");
                count++;
            }
            else if (userGuess < randNum) {
                System.out.println("Sorry your number was lower than the actual number");
                count++;
            }
            else {
                System.out.println("Yay! You got the correct answer");
                count++;
                break;
            }
        }
        System.out.println("You took "+ count + " tries to get the right answer!");
 	}
}

