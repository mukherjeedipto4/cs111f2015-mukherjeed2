
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Practical 5
//Date : 10 02 2015
//Diptajyoti Mukherjee
//Purpose : To check year for events using logical operators and if/else
//*********************************************
import java.util.Date;
import java.util.Scanner;
public class YearChecker {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n Practical #5\n " + new Date() +"\n");
        Scanner input = new Scanner(System.in);                                                 //creating an instance of scanner class
        int userYear;
        boolean leapYear;
        System.out.println("Enter a year");
        userYear = input.nextInt();
        // for determining if leap year or not
        if(userYear%4==0) {
            leapYear = true;
            if((userYear%100==0) && (userYear%400)!=0) {
                leapYear = false;
            }
        }
        else {
            leapYear = false;
        }
        //for printing out the result
        if(leapYear)
            System.out.println(userYear + " is a leap year");
        else
            System.out.println ( userYear + " is not a leap year" );
        // for determining brood II cicadas
        if(((userYear-2013)%17)==0)
            System.out.println(userYear + " is an emergence year for Brood II of 17 year Cicadas!" );
        else
            System.out.println(userYear + " is not an emergence year for Brood II of 17 year Cicadas");
        //for determining peak solar years
        if((userYear-2013)%11==0)
            System.out.println(userYear + " is a peak sunspot year");
        else
            System.out.println(userYear + " is not a peak sunspot year");
 	}
}

