
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Practical 4
//Date : 09 25 2015
//Diptajyoti Mukherjee
//Purpose : To create dialog boxes to ask the user for input and display output in dialog boxes.
//*********************************************
import java.util.Date;
import javax.swing.JOptionPane;
import java.util.Random;
public class CreateNewIdentity {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n " + new Date() +"\n");
        Random rand = new Random();                                 // to generate random numbers
        String firstName;                                           //holds first name
        String lastName;                                            //holds last Name
        String newName;                                             //holds modified last name and first name
        int newAge;                                                 //holds modified age
        int age;                                                    //holds original age
        String city;                                                //holds original city
        int yearOfBirth;                                            //holds original year of birth
        int newYear;                                                //holds new year of birth
        JOptionPane.showMessageDialog(null,"Let's get you a new identity!");
        firstName = JOptionPane.showInputDialog("What is your first name?");
        firstName = firstName+"tada";                                                   //changes first name
        lastName = JOptionPane.showInputDialog("What is your last name?");
        lastName = lastName + "kaboom";                                                 //changes last name
        newName = firstName + " " + lastName;                                           //holds changed names
        JOptionPane.showMessageDialog(null,"Your modified name is "+newName);
        age = Integer.parseInt(JOptionPane.showInputDialog("Enter your age "));
        newAge = (age-rand.nextInt(10));                                                //creates new age
        JOptionPane.showMessageDialog(null,"Your new age is "+newAge);
        city = JOptionPane.showInputDialog("Enter your city");
        city = city + "hula";                                                           //changes city name
        JOptionPane.showMessageDialog(null,"Your new city is "+city);
        yearOfBirth = Integer.parseInt(JOptionPane.showInputDialog("Enter your year of Birth"));
        newYear = (yearOfBirth-rand.nextInt(10));                                       //creates new year of birth
        JOptionPane.showMessageDialog(null,"New year of birth "+newYear);
 	}
}

