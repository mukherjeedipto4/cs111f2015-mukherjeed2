//*********************************************************************************
// CMPSC 111 Fall 2015
// Practical 9
//
// Purpose: Program demonstrating  illustrates how to call static methods a class
// from a method in the same class. Static methods are the methods that can be called
// without creating an instance of the object.
//*********************************************************************************

public class CallingMethodsInSameClass
{
    public static void main(String [] args)                                 //was missing [], added it
    {
        printOne();
        printOne();
        printTwo();
        // TO DO: add printThree() method call
        printThree();
    }

    public static void printOne()
    {
        System.out.println("Nothing is impossible, the word itself says \"I'm possible\" !");
    }

    public static void printTwo()
    {
        printOne();
        printOne();
    }

    // TO DO: write a new method called printThree() that calls method printTwo() one time

    public static void printThree() {
        printTwo();
    }
}
