//*********************************************************************************
// CMPSC 111 Fall 2015
// Practical 9
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        String day = s.next();          //fixed, scanner has next() not nextString()
        day = day.toLowerCase();


        // TO DO: Using a switch statement, given the day of the week, print whether it is a weekday or a weekend day.
        switch(day) {
            case "monday" :
            case "tuesday" :
            case "wednesday" :
            case "thursday" :
            case "friday" : System.out.println("weekday!");
                            break;
            case "saturday" :
            case "sunday" : System.out.println("weekend!");
                            break;
            default : System.out.println("enter proper input!");
                      break;
        }
    }
}
