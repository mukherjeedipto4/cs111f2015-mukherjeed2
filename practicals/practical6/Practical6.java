//====================================
// CMPSC 111
// Practical 6
// 15--16 October 2015
//Diptajyoti Mukherjee
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Diptajyoti Mukherjee\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Utensil spat;           // a kitchen utensil
        Octopus batocky;        //octopus 2
        Utensil knife;          //utensil 2

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        knife = new Utensil("Kitchen Knife");               //new utensil for practical
        knife.setColor("grey");                             //color for utensil
        knife.setCost(5.83);                                //cost of utensil

        ocky = new Octopus("Ocky",10);    // create and name the octopus
        //ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        batocky = new Octopus("BatOctopus",28);             //octopus 2 for practical
        batocky.setWeight(619);                             //weight for octopus 2
        batocky.setUtensil(knife);                          //utensil for octopus 2

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());

        //Printing out octopus 2's name and utensils
        System.out.println("\nTesting the new objects");
        System.out.println(batocky.getName() + " is "+ batocky.getAge()+" years old and weighs "+batocky.getWeight()
                + "pounds . His favorite Utensil is a "+ batocky.getUtensil());
        System.out.println("It costs "+ knife.getCost()+ "$ .It's color is : "+ knife.getColor());
    }
}
