
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Practical #8
//Date : 10 30 2015
//Diptajyoti Mukherjee
//Purpose :
//*********************************************
import java.util.Date;
import java.util.Random;
public class Practical8 {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n Practical #8\n " + new Date() +"\n");
        Random rand = new Random();
        int [] sumOfDices = new int[1000];
        int count = 0;
        int dice1,dice2;
        int maxSum , maxPos;
        while(count<1000) {
            dice1 = rand.nextInt(6)+1;
            dice2 = rand.nextInt(6)+1;
            System.out.println("Dice 1, Trial "+(count+1)+" : "+dice1);
            System.out.println("Dice 2, Trial "+(count+1)+" : "+dice2);

            sumOfDices[count] = dice1+dice2;
            count++;
        }
        count = 0;
        maxSum = sumOfDices[0];
        maxPos = 0;
        while(count<1000) {
            if(sumOfDices[count]>maxSum) {
                maxSum = sumOfDices[count];
                maxPos = count;
            }
            count++;
        }
        System.out.println("The maximum sum of two throws of dices found after 1000 trials is : "+maxSum + " and its simulated number is : "+(maxPos+1));

 	}
}

