
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Practical # 3
//Date : 09 11 2015
//Diptajyoti Mukherjee
//Purpose : Using Scanner to create a story
//*********************************************
import java.util.Date;    
import java.util.Scanner;
public class SuperheroStory {
	public static void main ( String [] args ) {			//main function starts here
		Scanner input = new Scanner(System.in);			//for input
		//declaring variables for the program
		String superhero;
		String villain;
		String anotherSuperhero;
		int number1;						//whole number for user input
		double number2;						//double number for user input
		double result;						//variable to store result of two numbers
		
		System.out.println("Name: Diptajyoti Mukherjee\n Practical # 3\n " + new Date() +"\n");
		System.out.println("Enter the name of a superhero");
		superhero = input.nextLine(); // for user input
		System.out.println("Enter the name of another superhero");
		anotherSuperhero = input.nextLine();
		System.out.println("Enter the name of the villain");
		villain = input.nextLine();
		System.out.println("Enter a whole number");
		number1 = input.nextInt();
		System.out.println("Enter any number");
		number2 = input.nextDouble();
		result = number1 * number2; //computing the result of two numbers
		System.out.println("A tale of two superheros\n\n");
		System.out.println(villain + " was very hungry and stole " + number1 + " eggs from Walmart which cost $"+number2+" each." + superhero + " and his friend " + anotherSuperhero + " came and defeated " + villain + ","); 
		System.out.println("saving $"+result+" for the store and eventually saving the day.");
		System.out.println("Go "+superhero+" and "+anotherSuperhero+"!");
 	}
} 
		
