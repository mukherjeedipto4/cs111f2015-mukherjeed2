package final_project;

import javax.swing.event.ListSelectionEvent;
import javax.swing.*;
import java.awt.*;
import javax.swing.event.ListSelectionListener;

public class AddClassToList implements ListSelectionListener {
	private JList <String> courseList;
	private JList <String> courseAdded;
	private DefaultListModel <String> courseListModel = new DefaultListModel<>();
	
	public AddClassToList(JList <String> cl, JList <String> ca) {
		courseList = cl;
		courseAdded = ca;
		
	}

	public void valueChanged(ListSelectionEvent e) {
		if(!e.getValueIsAdjusting()) {
			
			String s = (String)courseList.getSelectedValue();
			if(s!=null){								//to make sure there are no NullPointerExceptions
				courseListModel.addElement(s);
			}
			courseAdded.setModel(courseListModel);
			
		}
		

	}

}
