package final_project;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.event.ActionListener;

public class RemoveCourseAction implements ActionListener {

	private JList <String> addedCourses;
	private DefaultListModel<String> addedCoursesListModel;
	
	public RemoveCourseAction(JList <String> ca) {
		addedCourses = ca;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		addedCoursesListModel = (DefaultListModel<String>)addedCourses.getModel();
		int index = addedCourses.getSelectedIndex();
		addedCoursesListModel.removeElementAt(index);
		addedCourses.setModel(addedCoursesListModel);

	}

}
