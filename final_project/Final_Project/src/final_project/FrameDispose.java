package final_project;
import java.awt.event.*;
import javax.swing.*;
public class FrameDispose implements ActionListener{
	private JFrame frame;
	public FrameDispose(JFrame f) {
		frame = f;
		
	}
	public void actionPerformed(ActionEvent event) {
		frame.dispose();
	}

}
