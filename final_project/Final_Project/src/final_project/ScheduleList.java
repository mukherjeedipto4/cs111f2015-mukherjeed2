package final_project;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;
public class ScheduleList {
	private final String FILENAME = "Spring2016Class.txt";
	private ArrayList<ScheduleItem> courseSchedule;
	private int numOfCombinations;

	public ScheduleList() {
		courseSchedule = new ArrayList<ScheduleItem>();
	}

	public void getCoursesFromFile () throws IOException{
		File file = new File(FILENAME);
		Scanner scanFile = new Scanner(file);
		while(scanFile.hasNext()) {
			String courseString = scanFile.nextLine();
			Scanner scanCourseString = new Scanner(courseString);
			scanCourseString.useDelimiter(",");
			String dep = scanCourseString.next();
			String cName = scanCourseString.next();
			String div = scanCourseString.next();
			String sNum = scanCourseString.next();
			String eGroup = scanCourseString.next();
			String hLabs = scanCourseString.next();
			String ls = scanCourseString.next();
			String le = scanCourseString.next();
			String ld = scanCourseString.next();
			
			ScheduleItem sItem = new ScheduleItem(dep,cName,div,sNum,eGroup,hLabs,ls,le,ld);

			courseSchedule.add(sItem);
		}

	}
	
	public Iterator<String> getDepartmentList() {
		ScheduleItem [] cList = courseSchedule.toArray(new ScheduleItem[0]);
		ArrayList<String> department = new ArrayList<String>();
		for(int i = 0;i<cList.length;i++){
			if(i>0) {
				if(department.contains(cList[i].getDepartment()))
					continue;
			}
			department.add(cList[i].getDepartment());
		}
		return department.iterator();
	}
	
	public Iterator<String> getCoursesFromDept(String dept) {
		ScheduleItem [] cList = courseSchedule.toArray(new ScheduleItem[0]);
		ArrayList<String> course = new ArrayList<String>();
		for(int i =0;i<cList.length;i++){
			if(i>0){
				if(course.contains(cList[i].getCourseName()))
					continue;
			}
			if(dept.equalsIgnoreCase(cList[i].getDepartment()))
				course.add(cList[i].getCourseName());
		}
		return course.iterator();
	}
	
	public Iterator<String> getSectionsFromCourses(String courses) {
		ScheduleItem [] cList = courseSchedule.toArray(new ScheduleItem[0]);
		ArrayList<String> sections = new ArrayList<String>();
		for(int i = 0;i<cList.length;i++) {
			if(courses.equalsIgnoreCase(cList[i].getCourseName()))
				sections.add(cList[i].getSectionNumber());
		}
		return sections.iterator();
	}
	//Getting all the objects of type ScheduleItem which have the same coursename
	public Iterator<ScheduleItem> getObjectClassFromCourse(String [] cl) {
		String [] course = cl;
		ScheduleItem [] cList = courseSchedule.toArray(new ScheduleItem[0]);
		ArrayList<ScheduleItem> c = new ArrayList<ScheduleItem>();
		for(int i =0;i<course.length;i++) {
			for(int j = 0;j<cList.length;j++) {
				if(course[i].equalsIgnoreCase(cList[j].getCourseName()))
					c.add(cList[j]);
			}
		}
		return c.iterator();
	}
	
	//method utilising the combination algorithm to find non conflicting classes
	public ScheduleItem[][] findClassesThatWork (ScheduleItem [] courseArray,int numOfCourses) {
		Combination combination = new Combination(numOfCourses);
		ScheduleItem [] combinedCourses = new ScheduleItem[numOfCourses];
		combination.combinations2(courseArray,numOfCourses,0,combinedCourses);
		numOfCombinations = combination.getTotalNumberOfCombinations();
		return combination.getWorkingCourses();
		
	}
	//returning the number of combinations formed
	public int getNumberOfCombinations() {
		return numOfCombinations;
	}
	
}
