package final_project;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class ClassScheduling implements ActionListener{
	public void actionPerformed(ActionEvent event) {
		
		JFrame schedulingFrame = new JFrame("Class Scheduler");
		schedulingFrame.setVisible(true);
		schedulingFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		schedulingFrame.setLayout(new GridLayout(1,2));
		schedulingFrame.setSize(800,600);
		
		JPanel classSelectPanel = new JPanel();
		classSelectPanel.setLayout(new GridLayout(0,1));
		JPanel classesAddedPanel = new JPanel();
		schedulingFrame.add(classSelectPanel);
		schedulingFrame.add(classesAddedPanel);
		
		ScheduleList cList = new ScheduleList();
		try {
			cList.getCoursesFromFile();
		} catch (IOException e1) {
		
			System.out.println("error");
		}
		//getting the departments and adding them to the JList
		Iterator<String> it = cList.getDepartmentList();
		ArrayList<String>dept = new ArrayList<String>();
		
		while(it.hasNext())
			dept.add(it.next());
		
		
		String [] departments = dept.toArray(new String[0]);
		JList<String> departmentList = new JList<String>(departments);
		JScrollPane departmentScroll = new JScrollPane(departmentList); //scroll bars

		JList<String> courseList = new JList<String>();
		
		try {
			departmentList.addListSelectionListener(new ScheduleListSelection(departmentList,courseList));
		} catch (IOException e) {

		}
		JScrollPane courseScroll = new JScrollPane(courseList);
		courseScroll.setPreferredSize(new Dimension(100,100));
		classSelectPanel.add(departmentScroll);
		classSelectPanel.add(courseScroll);
		
		JList<String> addedCourses = new JList<String>();
		
		
		courseList.addListSelectionListener(new AddClassToList(courseList,addedCourses));
		classesAddedPanel.add(addedCourses);
		addedCourses.setPreferredSize(new Dimension(200,200));
		
		
		
		
		JButton removeCourseButton = new JButton("Remove");
		JButton checkScheduleButton = new JButton("Check Schedule!");
		//combo boxes to select major/minor/preferred class
		JComboBox<String> selectMajor = new JComboBox<String>();
		JComboBox<String> selectMinor = new JComboBox<String>();
		
		
		for(int i =0;i<departments.length;i++){
			selectMajor.addItem(departments[i]);
			selectMinor.addItem(departments[i]);
		}
		JLabel majorLabel = new JLabel("Select Major/Preferred Class:");
		JLabel minorLabel = new JLabel("Select Minor/Preferred Class:");
		classSelectPanel.add(majorLabel);
		classSelectPanel.add(selectMajor);
		classSelectPanel.add(minorLabel);
		classSelectPanel.add(selectMinor);
		
	
		classesAddedPanel.add(removeCourseButton);
		classesAddedPanel.add(checkScheduleButton);
		
		
		
		removeCourseButton.addActionListener(new RemoveCourseAction(addedCourses));
		try {
			checkScheduleButton.addActionListener(new CheckScheduleAction(addedCourses,selectMajor,selectMinor));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
	}

}
