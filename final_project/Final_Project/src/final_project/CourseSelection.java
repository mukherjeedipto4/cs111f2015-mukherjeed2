package final_project;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.io.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;

public class CourseSelection implements ListSelectionListener {
	private CourseDesList courses = new CourseDesList();
	private JTextArea description;
	private JList<String> courseList;
	public CourseSelection(JList<String> cList,JTextArea des) throws IOException {
		courseList = cList;
		description = des;
		courses.getCoursesFromFile();
	}
	@Override
	public void valueChanged(ListSelectionEvent e) {
		String course = (String)courseList.getSelectedValue();
		String text = new String();
		if(course!=null) 					//checks if user does not add null values
			text = courses.searchDescriptionFromCourseName(course);
		description.setText(text);

	}

}
