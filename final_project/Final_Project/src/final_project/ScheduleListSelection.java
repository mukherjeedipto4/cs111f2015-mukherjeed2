package final_project;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

import javax.swing.event.*;
import java.util.*;
public class ScheduleListSelection implements ListSelectionListener{
	private JList<String> departmentList;
	private JList<String> courseOfferedList;
	private ScheduleList courses = new ScheduleList();
	
	public ScheduleListSelection(JList<String> dl, JList<String> col) throws IOException {
		departmentList = dl;
		courseOfferedList = col;
		courses.getCoursesFromFile();
	}
	
	public void valueChanged(ListSelectionEvent e) {
		if(!e.getValueIsAdjusting()) {				//so that only one event, i.e. mouse press down or up is taken into account
			String s = (String)departmentList.getSelectedValue();
			DefaultListModel<String> courseListModel = new DefaultListModel<>();
			Iterator<String> it = courses.getCoursesFromDept(s);
			while(it.hasNext())
				courseListModel.addElement((String)it.next());
			courseOfferedList.setModel(courseListModel);
		}
	}

}
