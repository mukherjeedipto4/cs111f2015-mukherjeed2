package final_project;

public class ScheduleItem {
	private String department;
	private String courseName;
	private String division;
	private String sectionNumber;
	private String examGroup;
	private String hasLabs;
	private String labStartTime;
	private String labEndTime;
	private String labDay;


	public ScheduleItem(String dep,String cName,String div,String sNum,String eGroup,String hLabs,String ls, String le, String ld) {
		department = dep;
		courseName = cName;
		division = div;
		sectionNumber = sNum;
		examGroup = eGroup;
		hasLabs = hLabs;
		labStartTime = (ls);
		labEndTime = (le);
		labDay = ld;

	}

	public String getDepartment() {
		return department;
	}
	public String getCourseName() {
		return courseName;
	}
	public String getDivision() {
		return division;
	}
	public String getSectionNumber() {
		return sectionNumber;
	}
	public String getExamGroup() {
		return examGroup;
	}
	public String getHasLabs() {
		return hasLabs;
	}
	public String getLabStartTime() {
		return labStartTime;
	}
	public String getLabEndTime() {
		return labEndTime;
	}
	public String getLabDay() {
		return labDay;
	}


}
