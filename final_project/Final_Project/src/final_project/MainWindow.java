package final_project;
import javax.swing.*;
import java.awt.*;
public class MainWindow {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Main Window");
		frame.setLayout(new GridLayout(0,1));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton checkSchedule = new JButton("Check Schedule");
		JButton listOfClasses = new JButton("List of Classes");
		JButton help = new JButton("help");
		JButton exitWindow = new JButton("Exit");
		frame.add(checkSchedule);
		frame.add(listOfClasses);
		frame.add(help);
		frame.add(exitWindow);
		frame.setVisible(true);
		frame.setSize(600, 400);
		exitWindow.addActionListener(new ExitAction(exitWindow));
		help.addActionListener(new HelpAction());
		listOfClasses.addActionListener(new ListClassAction());
		checkSchedule.addActionListener(new ClassScheduling());
	
	}

}
