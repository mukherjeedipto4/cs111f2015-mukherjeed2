package final_project;
public class CourseDesItem {
	//Class for handling the course Description items
	private String division;
	private String department;
	private String courseName;
	private String description;
	
	public CourseDesItem(String div,String dept,String cName,String des) {
		division = div;
		department = dept;
		courseName = cName;
		description = des;
		
	}
	
	public String getDivision() {
		return division;
	}
	public String getDepartment() {
		return department;
	}
	public String getCourseName() {
		return courseName;
	}
	public String getDescription() {
		return description;
	}

}
