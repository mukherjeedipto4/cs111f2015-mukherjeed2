package final_project;
import javax.swing.*;
import java.awt.event.*;
public class ExitAction implements ActionListener {
	private JButton exitButton;
	
	public ExitAction(JButton b) {
		exitButton = b;
	}
	public void actionPerformed(ActionEvent event) {
		System.exit(0);
	}

}
