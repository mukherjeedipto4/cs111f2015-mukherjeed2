package final_project;

import java.awt.event.ActionEvent;
import java.text.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.*;

public class CheckScheduleAction implements ActionListener {
	private JList<String> addedCourses;
	private ScheduleList courseList = new ScheduleList();
	private Calendar cal = Calendar.getInstance();					//gets the calendar details from the computer
	private SimpleDateFormat d = new SimpleDateFormat("HH:mm");		//date format
	private JComboBox<String> majorList;
	private JComboBox<String> minorList;
	
	public CheckScheduleAction(JList<String> ac,JComboBox<String> maj,JComboBox<String> min) throws IOException {
		addedCourses = ac;
		courseList.getCoursesFromFile();
		majorList = maj;
		minorList = min;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//gets the selected major and minor from the JComboboxes
		String preferredMaj = (String)majorList.getSelectedItem();
		String preferredMin = (String)minorList.getSelectedItem();
		
		JFrame displayFrame = new JFrame("Results");
		displayFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		displayFrame.setVisible(true);
		//to get the number of courses
		DefaultListModel<String> courseModel = (DefaultListModel<String>)addedCourses.getModel();
		int numberOfCourses = courseModel.getSize();
		//Array to store courses
		String [] courseArray = new String[numberOfCourses];
		for(int i =0;i<numberOfCourses;i++) {
			if(courseModel.getElementAt(i)!=null) {
				courseArray[i] = courseModel.get(i);
			}
		}
		
		Iterator<ScheduleItem> it = courseList.getObjectClassFromCourse(courseArray);
		ArrayList<ScheduleItem> c = new ArrayList<ScheduleItem>();
		while(it.hasNext())
			c.add(it.next());
		ScheduleItem [] cArray = c.toArray(new ScheduleItem[0]);
		ScheduleItem[][] workingCourses =  courseList.findClassesThatWork(cArray,numberOfCourses);
		System.out.println(Arrays.toString(workingCourses));
	
		//text area to display the results
		JTextArea text = new JTextArea();
		String s = new String();
		//if courses are conflicting
		if(courseList.getNumberOfCombinations() == 0) {
			
			s = "Conflicting classes\n";
			s = s + getAlternateClass(preferredMaj,preferredMin);
		}
		//display non-conflicting results
		for(int i=0;i<courseList.getNumberOfCombinations();i++) {
			try {
				if(getLabsConflict(workingCourses[i],numberOfCourses)) {
					for(int j=0;j<numberOfCourses;j++) {
						s=s+workingCourses[i][j].getCourseName()+" ";
						s = s+getDaysFromExamGroup(workingCourses[i][j].getExamGroup())+" "+getTimeFromExamGroup(workingCourses[i][j].getExamGroup())+"-";
						String sTime = getTimeFromExamGroup(workingCourses[i][j].getExamGroup());
						Date dt = d.parse(sTime);
						cal.setTime(dt);
						if(getDaysFromExamGroup(workingCourses[i][j].getExamGroup()).equals("M W F")) 
							cal.add(Calendar.MINUTE, 50);	//classes on M,W,F are 50 mins long
						else
							cal.add(Calendar.MINUTE, 75);
						s=s+d.format(cal.getTime())+"\n";	//classes on Tu,Th are 75 mins long									
					}
					s=s+"-----------------------------------------------------\n";
				}
			} catch (ParseException e1) {
				System.out.println("error");
			}
		}
		if(s.equals(new String())) { 	//if there is a lab conflict
			s = getAlternateClass(preferredMaj,preferredMin);
		}
		text.setText(s);
		text.setEditable(false);
		JScrollPane textScroll = new JScrollPane(text);
		displayFrame.add(textScroll);
		displayFrame.pack();
		

	}
	
	private String getDaysFromExamGroup(String s) {			//Groups B,C,D,K have classes on Tu,Th. All others on M,W,F
		String days = new String();
		switch(s) {
		case "A" :
		case "E" :
		case "F" :
		case "G" :
		case "H" :
		case "I" :
		case "J" :
		case "L" : days = "M W F";
			break;
		default : days = "Tu Th";
		}
		return days;
	}
	
	private String getTimeFromExamGroup(String s) {		//getting the starting time for each class based on exam group
		String time = new String();
		switch(s) {
		case "A" : time = "09:00";
			break;
		case "B" : time = "14:30";
			break;
		case "C" : time = "09:30";
			break;
		case "D" : time = "11:00";
			break;
		case "E" : time = "08:00";
			break;
		case "F" : time = "13:30";
			break;
		case "G" : time = "10:00";
			break;
		case "H" : time = "08:00";
			break;
		case "I" : time = "14:30";
			break;
		case "J" : time = "11:00";
			break;
		case "K" : time = "13:30";
			break;
		case "L" : time = "15:30";
		default :  time = "n";
			break;
		}
		return time;
	}
	
	private boolean getLabsConflict(ScheduleItem [] sItems, int numCourses) throws ParseException{
		boolean scheduleWorks = true;
		for(int i = 0;i<numCourses;i++) {
			System.out.println(sItems[i].getHasLabs());
			if(sItems[i].getHasLabs().equalsIgnoreCase("true")) {
				for(int j =0;j<numCourses;j++) {
					if(i!=j) {
						String s = sItems[i].getLabDay();			//if the set contains a class that has an extra class/lab
						if(s.equals("Tu")||s.equals("Th")) {
							String t = sItems[j].getExamGroup();	//if it has exam group B,C,D,K, the method checks against conflicts on Tu,Th else on M,W,F
							if(t.equals("B")||t.equals("C")||t.equals("D")||t.equals("K")) {
								Date dtLab = d.parse(getTimeFromExamGroup(t));
								Calendar cLab = Calendar.getInstance();
								cLab.setTime(dtLab);
								Calendar cStart = Calendar.getInstance();
								Calendar cEnd = Calendar.getInstance();
								Date dtStart = d.parse(sItems[i].getLabStartTime());
								Date dtEnd = d.parse(sItems[i].getLabEndTime());
								cStart.setTime(dtStart);
								cEnd.setTime(dtEnd);
								cStart.add(Calendar.MINUTE,-75);
								
								if(cLab.compareTo(cEnd)==-1 && cLab.compareTo(cStart)==1) {
									scheduleWorks = false;
									break;
								}
							}
								
						}
						else {			//conflicting lab times on M,W,F
							String t = sItems[j].getExamGroup();
							if(t.equals("A")||t.equals("E")||t.equals("F")||t.equals("G")||t.equals("H")||t.equals("I")||t.equals("J")||t.equals("L")) {
								Date dtLab = d.parse(getTimeFromExamGroup(t));
								Calendar cLab = Calendar.getInstance();
								cLab.setTime(dtLab);
								Calendar cStart = Calendar.getInstance();
								Calendar cEnd = Calendar.getInstance();
								Date dtStart = d.parse(sItems[i].getLabStartTime());
								Date dtEnd = d.parse(sItems[i].getLabEndTime());
								cStart.setTime(dtStart);
								cEnd.setTime(dtEnd);
								cStart.add(Calendar.MINUTE,-75);
								
								if(cLab.compareTo(cEnd)==-1 && cLab.compareTo(cStart)==1) {
									scheduleWorks = false;
									break;
								}
							}
							
						}
					}
					else
						continue;
				}
				
			}
			else {
				continue;
			}
			if(!scheduleWorks)
				break;
			else
				continue;
		}
		return scheduleWorks;
	}
	//getting alternate classes the user may be interested in if the schedule does not work out
	public String getAlternateClass(String maj,String min) {
		Iterator<String> itMaj = courseList.getCoursesFromDept(maj);
		Iterator<String> itMin = courseList.getCoursesFromDept(min);
		String s = new String();
		s = s + "Suggested Courses\n";
		while(itMaj.hasNext())
			s = s + itMaj.next() + "\n";
		while(itMin.hasNext())
			s = s+ itMin.next() + "\n";
		
		return s;
	}
	
	

}
