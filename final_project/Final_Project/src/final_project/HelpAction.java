package final_project;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
public class HelpAction implements ActionListener {
	public void actionPerformed(ActionEvent event) {
		JFrame helpWindow = new JFrame("Help Window");
		helpWindow.setVisible(true);
		helpWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel textPanel = new JPanel();
		JPanel exitPanel = new JPanel();
		exitPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		helpWindow.setLayout(new GridLayout(0,1));
		String helpText = "Welcome to the class Scheduling Program\nTo see the list of classes offered at Allegheny along with\ntheir descriptions, click on List of Classes in the Main Window\nIf you want to check your schedule, click on Check Schedule and add the courses you want and then\nclick on check schedule. If the schedule is non-conflicting the result will be displayed in a new window. Otherwise you have to select\na new set of classes. Thank you for using this program!\n";
		JTextArea helpTextArea = new JTextArea(helpText);
		textPanel.add(helpTextArea);
		helpTextArea.setEditable(false);
		helpWindow.add(textPanel);
		JButton exitButton = new JButton("Exit");
		Dimension d = new Dimension(200,100);
		exitButton.setPreferredSize(d);
		exitPanel.add(exitButton);
		exitButton.addActionListener(new FrameDispose(helpWindow));
		helpWindow.add(exitPanel);
		helpWindow.pack();
	}

}
