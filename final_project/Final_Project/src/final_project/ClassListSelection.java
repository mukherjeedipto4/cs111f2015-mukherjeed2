package final_project;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;
import javax.swing.event.*;
public class ClassListSelection implements ListSelectionListener{ 
	private JList<String> list;
	private JList<String> list2;
	private CourseDesList courses = new CourseDesList();
	public ClassListSelection(JList<String> l,JList<String> l2) throws IOException {
		list = l;    //department list
		list2 = l2;	 //courses offered list
		courses.getCoursesFromFile();
	}
	public void valueChanged(ListSelectionEvent e) {
		String s = list.getSelectedValue(); 	
		DefaultListModel <String> list2Model = new DefaultListModel<>();
		Iterator<String> it = courses.searchCoursesFromDepts(s);
		ArrayList<String> cName = new ArrayList<String>();
		while(it.hasNext()) 			
			cName.add(it.next());
		String [] cNameList = cName.toArray(new String[0]);
		for(int i =0;i<cNameList.length;i++) //adding the courses offered under a department to the JList
			list2Model.addElement(cNameList[i]);
		list2.setModel(list2Model);
		
	}
	
}
