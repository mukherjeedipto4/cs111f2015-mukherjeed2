package final_project;

import java.util.*;
public class Combination {
	private final int MAX_COMBINATIONS = 2000;
	private ScheduleItem[][] wC = new ScheduleItem[MAX_COMBINATIONS][12]; 		//maximum classes per combination = 12 as there are 12 exam groups
    private int count =0;
    private int numCourses;
    
    public Combination(int numOfCourses) {
    	numCourses = numOfCourses;
    	
    }
	
	
	public void combinations2(ScheduleItem [] arr, int len, int startPosition, ScheduleItem [] result){
        if (len == 0){
            boolean duplicate = false;
            boolean duplicateExamGroup = false;
            for(int j = 0;j<(result.length-1);j++) {
                for(int k = j;k<(result.length);k++) {
                    if(k!=j && ((result[j].getDepartment().equalsIgnoreCase(result[k].getDepartment())))) {
                        duplicate = true;
                        break;
                    }
                }
            }
            //Initially checking for duplicate course names
            if(!duplicate) {
                for(int j = 0;j<result.length;j++) {
                    for(int k = j;k<result.length;k++) {
                        if(k!=j && ((result[j].getExamGroup().equalsIgnoreCase(result[k].getExamGroup())))) {
                            duplicateExamGroup = true;
                        }
                    }
                }
                //if no duplicate course names, then check for duplicate exam groups
                if(!duplicateExamGroup) {
                	for(int j =0;j<numCourses;j++) {
                		wC[count][j]=result[j];
                	}
                	count++;
                
                	return;
                }
            }

            return;
        }
       
        for(int i = startPosition; i <= arr.length-len; i++){
            result[result.length - len] = arr[i];
            combinations2(arr, len-1, i+1, result);
        }
        
    }
	
	public ScheduleItem[][] getWorkingCourses() {
		return wC;
	}
	public int getTotalNumberOfCombinations() {
		return count;
	}
	
}
