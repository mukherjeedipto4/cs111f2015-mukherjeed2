package final_project;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import java.io.IOException;
import java.awt.*;
public class ListClassAction implements ActionListener{
	//Class to list all classes and descriptions
	private CourseDesList courses = new CourseDesList();
	
	public void actionPerformed(ActionEvent event) {
		JFrame listClass = new JFrame("List of Classes Offered");
		listClass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		listClass.setVisible(true);
		listClass.setLayout(new GridLayout(0,2));
		JPanel classDisplayPanel = new JPanel();
		JPanel classDescriptionPanel = new JPanel();
		JPanel classDepartments = new JPanel();
		JPanel coursesOffered = new JPanel();
		classDisplayPanel.setLayout(new GridLayout(0,1));
		classDisplayPanel.add(classDepartments);
		classDisplayPanel.add(coursesOffered);
		listClass.add(classDisplayPanel);
		listClass.add(classDescriptionPanel);
		try {
			courses.getCoursesFromFile();
		} catch (IOException e) {
			System.out.println("Error!");
		}
		ArrayList<String> courseDepartments = new ArrayList<String>();
		Iterator<String> departmentIterator = courses.courseDepts();
		while(departmentIterator.hasNext()) {
			courseDepartments.add(departmentIterator.next());
		}
		
		String [] departments = courseDepartments.toArray(new String [0]);
		JList<String> classList = new JList<String>(departments);
		JList<String> courseList = new JList<String>();
	
		JTextArea text = new JTextArea();
		text.setPreferredSize(new Dimension(400,400));
		JScrollPane classScroll = new JScrollPane(classList);
		classDepartments.add(classScroll);
		classScroll.setPreferredSize(new Dimension(200,200));
		try {
			classList.addListSelectionListener(new ClassListSelection(classList,courseList));
		} catch (IOException e) {
			System.out.println("Error");
		}
		try {
			courseList.addListSelectionListener(new CourseSelection(courseList,text));
		} catch(IOException e) {
			System.out.println("Error");
		}
		JScrollPane courseScroll = new JScrollPane(courseList);
		coursesOffered.add(courseScroll);
		courseScroll.setPreferredSize(new Dimension(200,200));
		classDescriptionPanel.add(text);
		listClass.pack();
		
	}

}
