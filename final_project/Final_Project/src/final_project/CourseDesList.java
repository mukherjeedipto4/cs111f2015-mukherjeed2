package final_project;
import java.util.*;
import java.io.*;
public class CourseDesList{
	private ArrayList<CourseDesItem> courses;
	private final String FILENAME = "CourseListAndDescription.txt";
	
	public CourseDesList() {
		courses = new ArrayList<CourseDesItem>();
	}
	//getting the course description items from file
	public void getCoursesFromFile() throws IOException{
		File file = new File(FILENAME);
		Scanner scanFile = new Scanner(file);
		while(scanFile.hasNext()) {
			String fileString = scanFile.nextLine();
			Scanner scanString = new Scanner(fileString);
			scanString.useDelimiter(",");
			String div = scanString.next();
			String dept = scanString.next();
			String courseN = scanString.next();
			String des = scanString.next();
			CourseDesItem cItem = new CourseDesItem(div,dept,courseN,des);
			courses.add(cItem);
		}
		
	}
	public Iterator<String> courseDepts() {
		Iterator<CourseDesItem> it = courses.iterator();
		ArrayList<String> departments = new ArrayList<String>();
		int i =0;
		while(it.hasNext()) {
			String s = it.next().getDepartment();
			if(i>0) {
				
				if(departments.contains(s))
					continue;
			}
			departments.add(s);
			i++;
		}
		return departments.iterator();
		
	}
	//searching for course names under a particular dept
	public Iterator<String> searchCoursesFromDepts(String dept) {
		CourseDesItem [] cList = courses.toArray(new CourseDesItem[0]);
		ArrayList<String> course = new ArrayList<String>();
		for(int i =0;i<cList.length;i++) {
			if(i>0) {
				if(course.contains(cList[i].getCourseName()))
					break;
			}
			if(dept.equalsIgnoreCase(cList[i].getDepartment())) {
				course.add(cList[i].getCourseName());
			}
		}
		return course.iterator();
	}
	//getting descriptions from coursenames
	public String searchDescriptionFromCourseName(String cName) {
		CourseDesItem [] cList = courses.toArray(new CourseDesItem[0]);
		String des = new String();
		for(int i=0;i<cList.length;i++) {
			if(cName.equalsIgnoreCase(cList[i].getCourseName())) {
				des = cList[i].getDescription();
				break;
			}
		}
		return des;
	}
	public Iterator<CourseDesItem> getCourseItems() {
		return courses.iterator();
	}
	

}
