//*******************************************
//Computer Science 111 Fall 2015
//Lab #8
//Diptajyoti Mukherjee
//Date : 23 October 2015
//Honor Code : This work is mine unless cited
//Program Description : Class which defines various methods that can be used in the todolist
//********************************************
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {
	//declaring the instance variables
    private ArrayList<TodoItem> todoItems;				//arraylist of type TodoItems for storing the todolist items
    private static final String TODOFILE = "todo.txt";  //filename

    public TodoList() {
        todoItems = new ArrayList<TodoItem>(); 		    //constructor which creates an arraylist of objects of TodoItem class
    }

    public void addTodoItem(TodoItem todoItem) {		//mutator method which adds items to the arraylist
        todoItems.add(todoItem);
    }

    public Iterator getTodoItems() {
        return todoItems.iterator(); 					//accessor which gets converts the array into an iterator and returns it
    }

    public void readTodoItemsFromFile() throws IOException {    //method to read from file and throw exception if file does not exist
        Scanner fileScanner = new Scanner(new File(TODOFILE));  //taking inputs from file
        while(fileScanner.hasNext()) { 							//iterating through the file
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);    //creating new scanner with a String parameter
            todoScanner.useDelimiter(",");     					//reads only upto ','
            String priority, category, task; 					//to store the priority, category and the tasks from todolist
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task); //creating a new instance of TodoItem
            todoItems.add(todoItem); 									//adding the object to the arraylist
        }
    }

    public void markTaskAsCompleted(int toMarkId) {						//marks tasks as completed according to user input
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) { 									
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markCompleted();
            }
        }
    }

    public Iterator findTasksOfPriority(String requestedPriority) {
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks of the requestedPriority
        Iterator iterator = todoItems.iterator();								//converts the arraylist into an iterator for using in loop
        while(iterator.hasNext()) {
            TodoItem todoItem = (TodoItem)iterator.next(); 						
            if(todoItem.getPriority().equalsIgnoreCase(requestedPriority)) {
                priorityList.add(todoItem);										//adds the item to the arraylist
            }
        }
        return priorityList.iterator();
    }

    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        // TODO: Add source code that will find and return all tasks for the requestedCategory
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
             TodoItem todoItem = (TodoItem)iterator.next();
             if(todoItem.getCategory().equalsIgnoreCase(requestedCategory)) {
                categoryList.add(todoItem);										//adds the item to the arraylist
             }
        }
        return categoryList.iterator();
    }

    public String toString() {							//to display the list as string rather than as a list
        StringBuffer buffer = new StringBuffer(); 		//creates a mutable sequence of characters
        Iterator iterator = todoItems.iterator(); 		
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());  //coverts the iterator into a string and appends it to the buffer
            if(iterator.hasNext()) {
                buffer.append("\n"); 					//appends a newline to the buffer if the arraylist contains more items
            }
        }
        return buffer.toString();						//returns a string from the buffer
    }

}
