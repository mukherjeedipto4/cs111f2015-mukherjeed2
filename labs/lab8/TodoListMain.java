//*******************************************
//Computer Science 111 Fall 2015
//Lab #8
//Diptajyoti Mukherjee
//Date : 23 October 2015
//Honor Code : This work is mine unless cited
//Program Description : A todolist manager using three connected user created classes
//********************************************
import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class TodoListMain {

    public static void main(String[] args) throws IOException {                             //handling file errors
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, completed, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();                                 //object of TodoList class

        while(scanner.hasNext()) {                                          //for reading the todolist from the txt file
            String command = scanner.nextLine();
            if(command.equalsIgnoreCase("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equalsIgnoreCase("list")) {                               //printing the todolist in terminal
                System.out.println(todoList.toString());
            }
            else if(command.equalsIgnoreCase("completed")) {                          //mark tasks as completed
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsCompleted(chosenId);
            }
            else if(command.equalsIgnoreCase("priority-search")) {                    //search through list according to a given priority
                System.out.println("What is the priority of the task?");
                String chosenPriority = scanner.next();
                Iterator it_priority = todoList.findTasksOfPriority(chosenPriority);    //invoking priority search method of TodoList object
                while(it_priority.hasNext()) {                                          //iterating through the list to print search results
                    System.out.println(it_priority.next());
                }
            }
            else if(command.equalsIgnoreCase("category-search")) {                    //search through list according to a given category
            	System.out.println("What is the category of the task?");
            	String chosenCategory = scanner.next();
            	Iterator it_category = todoList.findTasksOfCategory(chosenCategory);   //invoking category search method of TodoList object
            	while(it_category.hasNext()) {
            		System.out.println(it_category.next());                           //iterating through the list to print search results
            	}
            }
            else if(command.equals("quit")) {
                break;
            }
        }

    }

}
