//*******************************************
//Computer Science 111 Fall 2015
//Lab #8
//Diptajyoti Mukherjee
//Date : 23 October 2015
//Honor Code : This work is mine unless cited
//Program Description : A class for each item in the todo list
//********************************************
public class TodoItem {

    private int id;
    private static int nextId = 0;                              //so all objects can share it
    private String priority;
    private String category;
    private String task;
    private boolean done;

    public TodoItem(String p, String c, String t) {             //constructor that intialises instance variables with parameters
        id = nextId;
        nextId++;                                               //incrementing nextId for next object of TodoItem class
        priority = p;
        category = c;
        task = t;
        done = false;
    }

    public int getId() {                //returns id
        return id;
    }

    public String getPriority() {      //returns priority
        return priority;
    }

    public String getCategory() {       //returns category
        return category;
    }

    public String getTask() {           //returns task
        return task;
    }

    public void markCompleted() {       //marks task as completed 
        done = true;
    }

    public boolean isCompleted() {      //returns booleans if task completed is true or false
        return done;
    }

    public String toString() {          //returns a string combining id priority and task and completed
        return new String(id + ", " + priority + ", " + category + ", " + task + ", completed? " + done);
    }

}
