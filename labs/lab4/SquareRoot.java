
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 04
//Date : 09 18 2015
//Diptajyoti Mukherjee
//Purpose : To compute the square root of a number by using guesses
//*********************************************
import java.util.Date;
import java.util.Scanner;
public class SquareRoot {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n Lab #\n " + new Date() +"\n");
        Scanner input = new Scanner(System.in);//for user input
        double number;
        double guess;
        double originalGuess;//to save the original guess
        System.out.println("Please enter a number");
        number = input.nextDouble();
        System.out.println("Please enter a guess for the root");
        guess = input.nextDouble();
        originalGuess = guess;
        //applying the formula to improve the guesses
        guess = 0.5*(guess + number/guess);
        guess = 0.5*(guess + number/guess);
        guess = 0.5*(guess + number/guess);
        guess = 0.5*(guess + number/guess);
        System.out.println("Original Guess : "+originalGuess+" Updated Guess : "+guess+" Guess squared : "+(guess*guess));


 	}
}

