
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 04
//Date : 09 18 2015
//Diptajyoti Mukherjee
//Purpose : To calculate the change for a value given by the user
//*********************************************
import java.util.Date;
import java.util.Scanner;
public class MakingChange {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n Lab # 04\n " + new Date() +"\n");
        Scanner input = new Scanner(System.in);
        int cents;
        int tens;
        int fives;
        int ones;
        int quarters;
        int dimes;
        int nickels;
        int pennies;
        System.out.println("Please enter money in cents ");
        cents = input.nextInt();
        System.out.println("You entered "+ cents + "cents");
        //converting into change
        tens = cents/1000;
        cents = cents%1000;
        fives = cents/500;
        cents = cents%500;
        ones = cents/100;
        cents = cents%100;
        quarters = cents/25;
        cents = cents%25;
        dimes = cents/10;
        cents = cents%10;
        nickels = cents/5;
        cents= cents%5;
        pennies = cents;
        System.out.println("Tens "+tens+", fives "+fives+" ,ones "+ones+" ,quarters "+quarters+" ,dimes "+dimes+" ,nickels "+nickels+" ,pennies "+pennies);
    }
}

