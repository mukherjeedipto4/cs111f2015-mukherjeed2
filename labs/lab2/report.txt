INITIAL ERRORS IN WordsofWisdom.java (First run) :

WordsofWisdom.java:12: error: <identifier> expected
	public class void main ( String [] args ) {
	            ^
WordsofWisdom.java:12: error: invalid method declaration; return type required
	public class void main ( String [] args ) {
	                  ^
WordsofWisdom.java:13: error: unclosed string literal
		System.out.println("Name: Diptajyoti Mukherjee\n Lab # 02\n " + new Date() +")
		                                                                            ^
WordsofWisdom.java:13: error: ';' expected
		System.out.println("Name: Diptajyoti Mukherjee\n Lab # 02\n " + new Date() +")
		                                                                              ^
WordsofWisdom.java:14: error: illegal start of expression
		System.out.println("\"Education is what remains after one has forgotten what one has learned in school.-Albert Einstein\"); //cited from google.com
		      ^
WordsofWisdom.java:14: error: ';' expected
		System.out.println("\"Education is what remains after one has forgotten what one has learned in school.-Albert Einstein\"); //cited from google.com
		          ^
WordsofWisdom.java:14: error: unclosed string literal
		System.out.println("\"Education is what remains after one has forgotten what one has learned in school.-Albert Einstein\"); //cited from google.com
		                   ^
WordsofWisdom.java:14: error: ';' expected
		System.out.println("\"Education is what remains after one has forgotten what one has learned in school.-Albert Einstein\"); //cited from google.com
		                                                                                                                                                   ^
WordsofWisdom.java:15: error: illegal start of expression
		System.out.println("\"If you spend too much time thinking about a thing, you'll never get it done. - Bruce Lee \"\n"); //cited from google.com
		      ^
WordsofWisdom.java:15: error: ';' expected
		System.out.println("\"If you spend too much time thinking about a thing, you'll never get it done. - Bruce Lee \"\n"); //cited from google.com
		          ^
WordsofWisdom.java:17: error: reached end of file while parsing
} 
 ^
11 errors

	

MOST CHALLENGING STEP : The most challenging step for me is writing the parameters inside the main function ( String [] args ) as I don't understand its function properly.

WHAT CAUSED THE ERRORS : There were three basic errors in the program which I found :
1 . In line 12, I mistakenly wrote 'public class void ... " instead of "public static void ... "
2. I missed a semi-colon at the end of line 13.
3. I missed a double quote the end of the println function in line 14/

VARIOUS VERSIONS OF println() FUNCTION :
The various println() methods differ in the fact that they take parameters of different data types. For example, public void println(float x) takes a floating data type as a parameter and prints the floating value number.
What I used in my WordsofWisdom program is println(String) as I am using a string as the parameter for the method.

COMMANDS TYPED IN TERMINAL FOR compiling and executing WordsofWisdom.java
javac WordsofWisdom.java
java WordsofWisdom
COMMANDS TYPED IN TERMINAL FOR compiling and executing WordsofWisdom2.java
javac WordsofWisdom2.java
java WordsofWisdom2


HONOR CODE :
This work is mine unless cited.
