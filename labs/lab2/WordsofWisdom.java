
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 02
//Date : 09 04 2015
//
//Purpose : Original version of Words of Wisdom 
//*********************************************
import java.util.Date; //importing to use new Date() 
public class WordsofWisdom {
	public static void main ( String [] args ) {		//main function
		System.out.println("Name: Diptajyoti Mukherjee\n Lab # 02\n " + new Date() +"\n");		//printing name and time/date
		System.out.println("\"Education is what remains after one has forgotten what one has learned in school.-Albert Einstein\""); //cited from google.com
		System.out.println("\"If you spend too much time thinking about a thing, you'll never get it done. - Bruce Lee \"\n"); //cited from google.com
 	}
} 
		
