
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 02
//Date : 09 03 2015
//
//Purpose : Lab exercise for Words of Wisdom 2 (Updated version of WoW usinf print() instead of println)
//*********************************************
import java.util.Date; //importing for using New Date()
public class WordsofWisdom2 {
	public static void main ( String [] args ) {		//main function
		System.out.print("Name: Diptajyoti Mukherjee\n Lab # 02\n " + new Date() +"\n");		//for name and date/time
		System.out.print("\"Education is what remains after one has forgotten what one has learned in school.-Albert Einstein\"\n"); //cited from google.com
		System.out.print("\"If you spend too much time thinking about a thing, you'll never get it done. - Bruce Lee \"\n"); //cited from google.com
 	}
} 
		
