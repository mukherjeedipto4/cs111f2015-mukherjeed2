
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 10
//Date : 11 06 2015
//Diptajyoti Mukherjee
//Purpose : To produce symmetric carpet designs using nested loops
//*********************************************
import java.util.Date;
public class CarpetDesign {
    static final int MAX_COLUMN = 32;           //maximum number of columns
    public static void main ( String [] args ) {
        System.out.println("Name: Diptajyoti Mukherjee\n Lab # 10\n " + new Date() +"\n");
        String design = args[0];
        drawLine(design);
        drawSquares(design);
        drawLine(design);
        drawDiamond(design);
        drawLine(design);
        drawSquares(design);
        drawLine(design);

    }
    public static void drawLine(String design) {            //to draw a set of 2 lines on different lines
        for(int i = 0; i<2 ; i++) {
            for(int j = 0; j<MAX_COLUMN; j++) {
                System.out.print(design);
            }
            System.out.print("\n");
        }

    }
    public static void drawSquares(String design) {         //to draw 2x2 squares
        for(int i = 0 ;i<2;i++) {
            for(int j =1;j<=MAX_COLUMN;j++) {
                if(j%3==0)                                  //to account for the spaces between the squares
                    System.out.print(" ");
                else
                    System.out.print(design);
            }
            System.out.print("\n");
        }

    }
    public static void drawDiamond(String design) {         //to draw a diamond
        for(int i = 1; i<=8;i++) {                          //for a triangle
            for(int j = 16;j>i;j--)                         //loop 1 : for the spaces which go up till half of the symmetric design
                System.out.print(" ");
            for(int k = 0 ; k<2*i-1;k++)                    //loop 2: prints a triangle
                System.out.print(design);
            System.out.print("\n");
        }
        for(int i = 8;i>0;i--) {                            //for an upside down triangle
            for(int j=16;j>i;j--)
                System.out.print(" ");                      //loop 3: reverses the order of spaces from that of loop 1
            for(int k = 0;k<2*i-1;k++)                      //loop 4: prints an upside down triangle
                System.out.print(design);
            System.out.print("\n");

        }

    }

}

