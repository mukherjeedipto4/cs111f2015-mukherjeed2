package lab9;
import java.util.*;
import org.jfugue.player.*;
import java.io.*;

public class MusicPlay {
	private final String FILENAME = "music.txt";
	private ArrayList<String> musicItems;
	private Random rand = new Random();
	
	public MusicPlay() {
		musicItems = new ArrayList<String>();
	}
	public void getMusicFromFile() throws IOException { 	//in order to read music strings from file
		File file = new File(FILENAME);
		Scanner scan = new Scanner(file);
		scan.useDelimiter("\n");							//every music string is stored on a different line
		while(scan.hasNext()) {
			String mString = scan.next();
			musicItems.add(mString); 						//adding the music strings to the arraylist
		}
		scan.close();
	}
	private String instrumentFinder(String ins) { 			//checking for a valid instrument and returning its instrument value
		if(ins.equalsIgnoreCase("piano"))
			return "I0";
		else if(ins.equalsIgnoreCase("celesta"))
			return "I8";
		else if(ins.equalsIgnoreCase("harmonica"))
			return "I22";
		else if(ins.equalsIgnoreCase("violin"))
			return "I40";
		else if(ins.equalsIgnoreCase("bass"))
			return "I32";
		else if(ins.equalsIgnoreCase("flute"))
			return "I73";
		else if(ins.equalsIgnoreCase("guitar"))
			return "I24";
		else {
			System.out.println("error! no instrument found. Using default"); 	//default is piano
			return "I0";
		}
	}
	private String tempoFinder(String temp) {			//checking for a valid tempo and returning its tempo value
		if(temp.equalsIgnoreCase("allegro"))
			return "T120";
		else if(temp.equalsIgnoreCase("moderato"))
			return "T95";
		else if(temp.equalsIgnoreCase("presto"))
			return "T180";
		else if(temp.equalsIgnoreCase("largo"))
			return "T45";
		else if(temp.equalsIgnoreCase("vivace"))
			return "T145";
		else {
			System.out.println("error! No such tempo found. Using default");	//default is allegro
			return "T120";
		}
	}
	public void findMusicAndPlay(String i, String t) {
		Player player = new Player();
		int randomMusic = rand.nextInt(musicItems.size());					//to get a random music string everytime
		String instrument = instrumentFinder(i);							//finds valid instrument value
		String tempo = tempoFinder(t);										//finds valid tempo value
		String mItem = musicItems.get(randomMusic);							//random music string
		System.out.println("Playing Notes...");
		System.out.println(mItem);											//displaying the notes that are being played
		String playString = instrument + " " + tempo + " " +mItem;			//the final music string that will be played with specified instruments and temp
		player.play(playString);
		}
}
	
	

