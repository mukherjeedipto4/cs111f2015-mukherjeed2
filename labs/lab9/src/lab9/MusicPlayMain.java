package lab9;
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 09
//Date : 11 05 2015
//Diptajyoti Mukherjee
//Purpose : to play a random piece of music using a specified instrument and tempo
//*********************************************
import java.util.*;
import java.io.IOException;

public class MusicPlayMain {
	public static void main(String [] args) throws IOException { 
		System.out.println("Name: Diptajyoti Mukherjee\n Lab #09\n " + new Date() +"\n");
		MusicPlay playMusic = new MusicPlay();
		Scanner input = new Scanner(System.in);
		String instrument,tempo,choice;
		do	{
			System.out.println("Please type your choice of instrument");
			System.out.println("Your choices are bass , piano , guitar , flute , harmonica , violin , celesta");
			playMusic.getMusicFromFile(); 					//reading music strings from file
			instrument = input.next();
			System.out.println("Please enter your choice of tempo");
			System.out.println("Your choices are moderato , allegro , presto , largo , vivace");
			tempo = input.next();
			playMusic.findMusicAndPlay(instrument,tempo); 	//finding a random piece of music and playing it
			System.out.println("Do you want to play again? (yes/no)");
			choice = input.next();
			if(choice.equalsIgnoreCase("no")) { 			//in order to break from the loop
				System.out.println("Thank you for using this program!");
				break;
			}
		}while(true); //so that loop continues to run
		
		input.close();
	}

}
