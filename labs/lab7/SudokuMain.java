
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 07
//Date : 10 16 2015
//Diptajyoti Mukherjee
//Purpose : To test if a 4x4 sudoku is valid or not
//*********************************************
import java.util.Date;
public class SudokuMain {
    public static void main ( String [] args ) {
		System.out.println("Name: Diptajyoti Mukherjee\n Lab # 07\n " + new Date() +"\n");
        SudokuChecker sudoku = new SudokuChecker();					//creating a new instance of SudokuChecker
        sudoku.getGrid();											//getting input from user
        sudoku.checkGrid();											//checking user input for valid sudoku grid
 	}
}

