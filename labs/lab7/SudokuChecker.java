//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 07
//Date : 10 16 2015
//Diptajyoti Mukherjee
//Purpose : To test if a 4x4 sudoku is valid or not
//*********************************************
import java.util.Scanner;
public class SudokuChecker {
    //Instance variables
    private int w1,w2,w3,w4,x1,x2,x3,x4,y1,y2,y3,y4,z1,z2,z3,z4;
    private Scanner input = new Scanner(System.in);
    // constructor
    public SudokuChecker(){
        System.out.println("Welcome to Sudoku Checker! Please enter the sudoku as per the instructions below!"
                +"Hit enter after each row and please leave a space between each number");
        w1=w2=w3=w4=x1=x2=x3=x4=y1=y2=y3=y4=z1=z2=z3=z4=0;
    }
    public void getGrid() {                             //to get sudoku values from user
        System.out.print("Enter row 1: ");
        w1 = input.nextInt();
        w2 = input.nextInt();
        w3 = input.nextInt();
        w4 = input.nextInt();
        System.out.print("\nEnter row 2: ");
        x1 = input.nextInt();
        x2 = input.nextInt();
        x3 = input.nextInt();
        x4 = input.nextInt();
        System.out.print("\nEnter row 3: ");
        y1 = input.nextInt();
        y2 = input.nextInt();
        y3 = input.nextInt();
        y4 = input.nextInt();
        System.out.print("\nEnter row 4: ");
        z1 = input.nextInt();
        z2 = input.nextInt();
        z3 = input.nextInt();
        z4 = input.nextInt();
    }
    public void checkGrid() {                                       //check if each row, column and region adds up to 10
        int reg1,reg2,reg3,reg4,row1,row2,row3,row4,col1,col2,col3,col4;
        reg1 = w1+w2+x1+x2;
        reg2 = y1+y2+z1+z2;
        reg3 = w3+w4+x3+x4;
        reg4 = y3+y4+z3+z4;
        row1 = w1+w2+w3+w4;
        row2 = x1+x2+x3+x4;
        row3 = y1+y2+y3+y4;
        row4 = z1+z2+z3+z4;
        col1 = w1+x1+y1+z1;
        col2 = w2+x2+y2+z2;
        col3 = w3+x3+y3+z3;
        col4 = w4+x4+y4+z4;
        if(reg1==10)
            System.out.println("REG-1 : GOOD");
        else
            System.out.println("REG-1 : BAD");
        if(reg2==10)
            System.out.println("REG-2 : GOOD");
        else
            System.out.println("REG-2 : BAD");
        if(reg3==10)
            System.out.println("REG-3 : GOOD");
        else
            System.out.println("REG-3 : BAD");
        if(reg4==10)
            System.out.println("REG-4 : GOOD");
        else
            System.out.println("REG-4 : BAD");
        if(row1==10)
            System.out.println("ROW-1 : GOOD");
        else
            System.out.println("ROW-1 : BAD");
        if(row2==10)
            System.out.println("ROW-2 : GOOD");
        else
            System.out.println("ROW-2 : BAD");
        if(row3==10)
            System.out.println("ROW-3 : GOOD");
        else
            System.out.println("ROW-3 : BAD");
        if(row4==10)
            System.out.println("ROW-4 : GOOD");
        else
            System.out.println("ROW-4 : BAD");
        if(col1==10)
            System.out.println("COL-1 : GOOD");
        else
            System.out.println("COL-1 : BAD");
        if(col2==10)
            System.out.println("COL-2 : GOOD");
        else
            System.out.println("COL-2 : BAD");
        if(col3==10)
            System.out.println("COL-3 : GOOD");
        else
            System.out.println("COL-3 : BAD");
        if(col4==10)
            System.out.println("COL-4 : GOOD");
        else
            System.out.println("COL-4 : BAD");
        if((reg1==10)&&(reg2==10)&&(reg3==10)&&(reg4==10)&&(row1==10)&&(row2==10)&&(row3==10)&&(row4==10)&&(col1==10)&&(col2==10)&&(col3==10)&&(col4==10)) {        //to check if it each row, col and reg satisfies the condition
             System.out.println("SUDOKU : VALID");
        }
        else {
            System.out.println("SUDOKU : NOT VALID");
        }
    }

}
