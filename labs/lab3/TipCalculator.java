
//**********************************************
//Honor Code : This work is mine unless cited
//CMPSC 111 Fall 2015
//Lab # 03
//Date : 09 11 2015
//Diptajyoti Mukherjee
//Purpose : Creating a Tip Calculator
//*********************************************
import java.util.Date;
import java.util.Scanner;
import java.text.NumberFormat; 
public class TipCalculator {
	public static void main ( String [] args ) {							//main function
		System.out.println("Name: Diptajyoti Mukherjee\n Lab # 03\n " + new Date() +"\n");
		Scanner userInput = new Scanner(System.in);		//for user input
		String name;		//user's name
		float bill;		//initial bill	
		float tip_percentage;	//bill % out of 100
		float total_bill;	//bill with tip
		float tip;		//to store tip
		int numPeople;		//total number of people sharing the amount
		float share;		//each person's share
		NumberFormat formatNum = NumberFormat.getCurrencyInstance();		//used to format into currency format, got help from book
		System.out.println(" Hi! Please enter your name");		
		name = userInput.nextLine();				//to get input from user
		System.out.println(name + " welcome to ABC restaurant! ");
		System.out.println("Please enter your bill amount ");
		bill = userInput.nextFloat();				// to get bill amount
		System.out.println("Please enter your desired tip percentage (out of 100) ");
		tip_percentage = userInput.nextFloat();			//tip %
		tip = (tip_percentage/100)*bill;			//calculating the tip
		total_bill = tip + bill;				//calculating total bill
		System.out.println("Original Amount : "+formatNum.format(bill)+ "\nTip amount : "+formatNum.format(tip)+"\nTotal bill : "+formatNum.format(total_bill));	//used formatNum to format the number into
																						//currency format, from book sec 3.6
		System.out.println("Please enter the number of people splitting up the bill");
		numPeople = userInput.nextInt();			//taking user input for number of people
		share = (total_bill/numPeople);				//calculating each person's share
		System.out.println("Each person's share is : " + formatNum.format(share));
		System.out.println("Thank you! Please visit us again!");
 	}
} 
		
