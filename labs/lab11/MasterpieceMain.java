//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Diptajyoti Mukherjee
// CMPSC 111 Fall 2015
// Lab 11
// Date: 11 13 2015
//
// Purpose: To use java graphics to draw a scenery on the canvas
//=================================================
import java.awt.*;
import javax.swing.*;

public class MasterpieceMain extends JApplet
{

    public void paint(Graphics page)
    {
        Masterpiece masterpiece = new Masterpiece(page,600,400,Color.cyan);
        masterpiece.setBackgroundColor();
        masterpiece.drawMountain();
        //loop to draw four clouds at a specific height
        for(int i = 0,x=60;i<4;i++,x+=150) {
            masterpiece.drawCloud(x,60);
        }
        masterpiece.drawGrass();
        masterpiece.drawRiver();
        masterpiece.drawSun();
        masterpiece.drawTree();
        masterpiece.drawStr(70,20);

    }

    // main method that runs the program
    public static void main(String[] args)
    {
        JFrame window = new JFrame(" Dipto's masterpiece ");

        	window.getContentPane().add(new MasterpieceMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		    window.setSize(600, 400);

    }
}
