//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Diptajyoti Mukherjee
// CMPSC 111 Fall 2015
// Lab 11
// Date: 11 13 2015
//
// Purpose: Use java graphics to draw a scenery on the canvas
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{
    // instance variables
    private Graphics p;
    private int width;
    private int height;
    private Color backgroundColor;
    // constructor
    public Masterpiece(Graphics page,int w,int h,Color bc) {
        p = page;
        width = w;
        height = h;
        backgroundColor = bc;

    }

    // methods
    public void setBackgroundColor() {
        p.setColor(backgroundColor);
        p.fillRect(0,0,width,height);
    }
    public void drawCloud(int x,int y) {
    	p.setColor(Color.white);
    	p.fillOval(x-10,y-10,20,20);
    	p.fillOval(x,y-15,20,20);
    	p.fillOval(x,y-5,20,20);
    	p.fillOval(x+10,y-15,20,20);
    	p.fillOval(x+10,y-5,20,20);
    	p.fillOval(x+20,y-10,20,20);

    }
    public void drawGrass() {
    	p.setColor(Color.green);
    	p.fillRect(0,200,600,200);
    }
    public void drawRiver() {
    	p.setColor(Color.blue);
        //set of points for drawing the polygon
    	int [] ypoints = {200,200,400,400};
    	int [] xpoints = {400,450,500,300};
    	p.fillPolygon(xpoints,ypoints,4);
        p.setColor(Color.white);
        p.drawLine(450,300,455,320);
        p.drawLine(400,350,395,370);
        p.drawLine(370,270,363,290);
        p.drawLine(450,250,456,270);
        p.drawLine(425,210,425,220);
        p.drawLine(425,300,425,320);
        p.drawLine(425,350,425,370);
        p.drawLine(380,300,375,320);
    }
    public void drawSun() {
    	p.setColor(Color.yellow);
    	p.fillArc(-40,-40,75,75,0,-90);


    }
    public void drawTree() {
        p.setColor(new Color(122,60,43));
        p.fillRoundRect(100,225,30,120,3,30);
        p.setColor(new Color(34,82,19));
        p.fillOval(75,200,55,55);
        p.fillOval(95,200,55,55);
        p.fillOval(85,225,30,30);
        p.fillOval(75,150,55,55);
        p.fillOval(95,150,55,55);
        p.fillOval(100,145,30,30);
        p.fillOval(60,190,40,40);
        p.fillOval(120,190,40,40);


    }
    public void drawMountain() {
    	p.setColor(Color.gray);
        //set of points (x,y) for drawing the triangular mountains
        int [] xpoints = {150,0,300};
        int [] ypoints = {100,200,200};
        p.fillPolygon(xpoints,ypoints,3);
        //set of points (x,y) for drawing another triangular mountain
        int [] xpoints2 = {450,300,600};
        int [] ypoints2 = {100,200,200};
        p.fillPolygon(xpoints2,ypoints2,3);

    }

    public void drawStr(int x, int y) {
         p.setColor(Color.black);
         String str = "Beauty lies in the eyes of the beholder - Plato";
         p.drawString(str,x,y);
    }
}

